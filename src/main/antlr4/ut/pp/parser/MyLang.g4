grammar MyLang;

program : statement+;
statement : assignment | if | while | print | thread | IDENTIFIER '.' lock;
assignment : type IDENTIFIER '=' expr | type IDENTIFIER | 'global' type IDENTIFIER '=' expr | 'global' type IDENTIFIER | IDENTIFIER '=' expr;
if : 'if' predicate '{' (expr | statement) '}' | 'if' predicate '{' (expr | statement) '}' 'else' '{' (expr | statement)'}';
while : 'while' predicate '{' (expr | statement)'}';
print : 'print' (expr | statement);
thread : 'thread {' statement+ '}';
lock : 'lock' | 'unlock';

expr : literal | IDENTIFIER | expr binop expr | '(' expr ')';
literal : INT | STRING | BOOL;
binop : '+' | '-' | '*';
ordering : '<' | '==' | '!=' |'>';
predicate : '(' expr ordering expr ')';
type : 'int' | 'string' | 'bool' | 'thread' | 'lock' ;



INT: [0-9]+;
IDENTIFIER: [A-Z][a-zA-Z0-9_]*;
STRING:'"' [a-zA-Z0-9_]* '"';
BOOL: 'true' | 'false';
COMMENT : '//' (~('\n'))* -> skip;
WS : [ \n\t\r]+ -> skip;