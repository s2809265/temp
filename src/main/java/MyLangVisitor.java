// Generated from /Users/zsomborivanyi/UT/Module8/maven-my-lang/src/main/antlr4/ut/pp/parser/MyLang.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MyLangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MyLangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MyLangParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MyLangParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(MyLangParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(MyLangParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(MyLangParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile(MyLangParser.WhileContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(MyLangParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#thread}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThread(MyLangParser.ThreadContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#lock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLock(MyLangParser.LockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(MyLangParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(MyLangParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#binop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinop(MyLangParser.BinopContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#ordering}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering(MyLangParser.OrderingContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#predicate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(MyLangParser.PredicateContext ctx);
	/**
	 * Visit a parse tree produced by {@link MyLangParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(MyLangParser.TypeContext ctx);
}